package com.example.simon_000.myyearbook2;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class CurrentWeather extends AppCompatActivity {

    private FusedLocationProviderClient mFusedLocationClient;

    TextView locationTV, day1TV, day2TV, day3TV, day4TV, day5TV;
    double mLon = 0;
    double mLat = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_weather);

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new 	StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        //Should prompt the user, called in the launch activity too
        checkPermission();

        day1TV = findViewById(R.id.weatherTV1);
        day2TV = findViewById(R.id.weatherTV2);
        day3TV = findViewById(R.id.weatherTV3);
        day4TV = findViewById(R.id.weatherTV4);
        day5TV = findViewById(R.id.weatherTV5);
        locationTV = findViewById(R.id.locationTV);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        try {
            //Get the last location
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        //Called from another thread i think
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                mLat = location.getLatitude();
                                mLon = location.getLongitude();
                                locationTV.setText("Lon, Lat : "+ String.valueOf(mLon) + ", "+String.valueOf(mLat));
                                //Here we pass the gotten location to the find_weather funtion
                                find_weather(mLat, mLon);
                            }
                        }
                    });
        }catch(SecurityException e){
            e.printStackTrace();
        }
    }

    //Check if we have the needed location permissions for getting the location, should prompt user if need be
    public void checkPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                ){//Can add more as per requirement

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    123);
        }
    }

/*
    Finds the weather for the given longitude and latitude coordinates, returns 5 day forecast :
    {
        "cod": "200",
            "message": 0.0046,
            "cnt": 40,
            "list": [
        {
            "dt": 1520726400,
                "main": {
            "temp": 35.17,
                    "temp_min": 34.94,
                    "temp_max": 35.17,
                    "pressure": 1024.67,
                    "sea_level": 1028.27,
                    "grnd_level": 1024.67,
                    "humidity": 90,
                    "temp_kf": 0.13
        },
            "weather": [
            {
                "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
            }
            ],
            "clouds": {
            "all": 80
        },
            "wind": {
            "speed": 8.41,
                    "deg": 291.002
        },
            "sys": {
            "pod": "n"
        },
            "dt_txt": "2018-03-11 00:00:00"
        },
          39 more of these reports, 8 per day
*/

    public void find_weather(double lat, double lon){
        String url = "http://api.openweathermap.org/data/2.5/forecast?lat="+String.valueOf(lat)+
                "&lon=" + String.valueOf(lon) + "&appid=59813932ce84f06608c9f54c95ab783a&units=imperial\n";

        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{

                    JSONArray list_array = response.getJSONArray("list");
                    JSONObject day1 = list_array.getJSONObject(0);
                    JSONObject day2 = list_array.getJSONObject(8);
                    JSONObject day3 = list_array.getJSONObject(16);
                    JSONObject day4 = list_array.getJSONObject(24);
                    JSONObject day5 = list_array.getJSONObject(32);

                    double temp1 = day1.getJSONObject("main").getDouble("temp");
                    double temp2 = day2.getJSONObject("main").getDouble("temp");
                    double temp3 = day3.getJSONObject("main").getDouble("temp");
                    double temp4 = day4.getJSONObject("main").getDouble("temp");
                    double temp5 = day5.getJSONObject("main").getDouble("temp");

                    day1TV.setText("Now Temp  : " + String.valueOf(temp1));
                    day2TV.setText("+24hr Temp: " + String.valueOf(temp2));
                    day3TV.setText("+2d Temp  : " + String.valueOf(temp3));
                    day4TV.setText("+3d Temp  : " + String.valueOf(temp4));
                    day5TV.setText("+4d Temp  : " + String.valueOf(temp5));

                }catch(JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
                @Override
                        public void onErrorResponse(VolleyError error){
                    //panic
                }
        }
        );

        //This is where the new spread is spun off, I spin the weather request from the bottom of
        //the location request
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jor);
    }

}

