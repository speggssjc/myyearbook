package com.example.simon_000.myyearbook2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MyResume extends AppCompatActivity {
        private WebView mWebview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_resume);
        mWebview = (WebView)findViewById(R.id.resumeWebView);
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.loadUrl("http://www.nytimes.com");
        mWebview.setWebViewClient(new WebViewController());
    }

    public class WebViewController extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
